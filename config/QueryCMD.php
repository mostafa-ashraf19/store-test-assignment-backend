<?php 

include_once 'Database.php';

class Query {
    
    protected $conn;
    private $table;

    public function __construct($table)
    {
        $this->conn = (new Database)->connect();
        $this->table = $table;
    }

    public function read() {

        $query = 'SELECT id ,sku, name, price, type,specs FROM ' . $this->table . ' ORDER BY id ASC';

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    public function delete_product($sku)
    {

        $query = 'DELETE FROM ' . $this->table . ' WHERE sku= :sku';

        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':sku', $sku);

        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
    public function column_value_exists($column, $value) {
        $query = 'SELECT sku FROM ' .$this->table. ' WHERE '.$column.'= :value';

        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':value', $value);

        $stmt->execute();

        return $stmt;
    }
    public function create($sku, $name, $price, $type, $specs)
    {

        if (is_string($price)) {
            $price = intval($price);
        }

        $query = 'INSERT INTO ' . $this->table . ' SET sku = :sku, name = :name, price = :price, type = :type, specs = :specs';


        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':sku', $sku);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':price', $price);
        $stmt->bindParam(':type', $type);
        $stmt->bindParam(':specs', $specs);

        try {
            if ($stmt->execute()) {
                return true;
            }
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage();
        }

        return false;
    }
}