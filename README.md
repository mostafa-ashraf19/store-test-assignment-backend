# Scandiweb assignment -  Products Store Backend

Pure PHP backend that's uses MySQL for databases for handling add products or massive deleting for products.

A user is can add three main products types like `DVD`, `BOOK` and  `Furniture`. The application support making massive delete for products.

The connection between frontend and backend did by *REST API*

## REST API

1. **read** 
    > apis/read
2. **create**
    > apis/read
3. **delete**
    > apis/read
## Data

There are two types of products stored in database:

* DVD Product
* Book Product
* Furniture Product

### Products

Products include:

| Attribute    | Type             |
|-----------------|------------------|
| sku                | String           |
| name          | String           |
| price  | number           | 
| type | string |
| specs      | size or weigth or "width, length heigth" |

### Validations

> SKUs must be unique. 
> All product values must be available and suitable.