<?php

include_once 'AbstractProduct.php';
include_once 'Book.php';
include_once 'Furniture.php';
include_once 'DVD.php';

class Validator
{
    private $is_valid = false;
    private $inputs;

    public function __construct($inputs)
    {

        $this->inputs = $inputs;
        
        switch ($this->inputs->type) {
            case '0':
                $this->Validate_and_insert(new Furniture($inputs));
                break;

            case '1':
                $this->Validate_and_insert(new Book($inputs));
                break;

            case '2':
                $this->Validate_and_insert(new DVD($inputs));
                break;

            default:
                break;
        }
    }

    public function Validate_and_insert(AbstractProduct $product)
    {
        $this->is_valid = $product->validate_inputs();

        if ($this->is_valid  && $product->insert()) {
            echo json_encode(
                array('message' => 'Product Created', 'status' => 200, 'product' => $product->getInfo())
            );
        } else {
            echo json_encode(
                array('message' => 'Product Not Created', 'status' => 400)
            );
        }
    }
}
