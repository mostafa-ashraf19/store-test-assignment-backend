<?php

include_once '../config/QueryCMD.php';

class Product extends Query
{

    private $table = 'products';

    // Product Properties.
    protected $sku;
    protected $name;
    protected $price;
    protected $specs;
    protected $type;


    public function __construct()
    {
        parent::__construct($this->table);
    }

    function show()
    {
        $results = $this->read();

        // Products array
        $products_arr = array();

        while ($row = $results->fetch(PDO::FETCH_ASSOC)) {
            extract($row);

            $product_item = array(
                'id' => $id,
                'sku' => $sku,
                'name' => $name,
                'price' => $price,
                'type' => $type,
                'specs' => $specs,

            );
            array_push($products_arr, $product_item);
        }

        return $products_arr;
    }

    function insert()
    {
        return $this->create($this->sku, $this->name, $this->price, $this->type, $this->specs);
    }

    function delete($sku)
    {
        return $this->delete_product($sku);
    }

    public function getInfo()
    {
        return (object) array(
            'sku' => $this->sku,
            'name' => $this->name, 'price' => $this->price,
            'type' => $this->type, 'specs' => $this->specs
        );
    }

    function validate_inputs()
    {
        if ($this->validate_SKU_uniqueness()) {
            return true;
        }
        return false;
    }

    function validate_SKU_uniqueness()
    {
        return $this->column_value_exists('sku', $this->sku)->rowCount() == 0;
    }
}
