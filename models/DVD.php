<?php

include_once 'Product.php';
include_once 'AbstractProduct.php';

class DVD extends Product implements AbstractProduct
{

    public function __construct($inputs)
    {
        parent::__construct();

        $this->sku = $inputs->sku;
        $this->name = $inputs->name;
        $this->price = $inputs->price;
        $this->type = $inputs->type;
        $this->specs = $inputs->specs;
    }

}
