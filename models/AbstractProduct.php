<?php 

interface AbstractProduct {
    public function validate_inputs();
    public function insert();
    public function getInfo();
    public function validate_SKU_uniqueness();
}