<?php

include_once '../config/QueryCMD.php';

include_once 'Product.php';
include_once 'Validator.php';

class Products
{

    public function getAll()
    {
        return (new Product)->show();
    }

    public function insert($inputs)
    {
        $validator = new Validator($inputs);
    }

    public function remove_one($sku)
    {
        return (new Product)->delete($sku);
    }

    public function remoreProducts($skus)
    {
        array_map(function ($sku) {
            if (!$this->remove_one($sku)) {
                return false;
            }
        }, $skus);
        return true;
    }
}
