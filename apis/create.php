<?php

// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../models/Products.php';

// Product raw data.  
$sku = isset($_GET['sku']) ? $_GET['sku'] : die();
$name = isset($_GET['name']) ? $_GET['name'] : die();
$price = isset($_GET['price']) ? $_GET['price'] : die();
$type = isset($_GET['type']) ? $_GET['type'] : die();
$specs = isset($_GET['specs']) ? $_GET['specs'] : die();

$data = (object) array(
    'sku' => $sku,
    'name' => $name, 'price' => $price,
    'type' => $type, 'specs' => $specs
);

(new Products)->insert($data);
