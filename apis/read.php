<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../models/Products.php';

$products = new Products;

$stored_products = $products->getAll();

if (sizeof($stored_products)) {
  echo json_encode(array('products' => $stored_products));
} else {
  echo json_encode(array('message' => 'No Products Found'));
}
