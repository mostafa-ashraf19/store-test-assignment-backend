<?php
// Headers

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../models/Products.php';

$skus = isset($_GET['skus']) ? $_GET['skus'] : die();

if ((new Products)->remoreProducts($skus)) {
    echo json_encode(
        array('message' => 'Selected Products Deleted', 'status' => 200)
    );
} else {
    echo json_encode(
        array('message' => 'Selected Products Not Deleted', 'status' => 400)
    );
}
